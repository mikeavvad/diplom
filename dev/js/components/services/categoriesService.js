var categ_service = angular.module('categoriesServiceModule', ['restangular']);

categ_service.service('categoriesService', ['Restangular', function(Restangular) {
	Restangular.setBaseUrl(base_url);

	this.getCategories = function() {
		var categories = Restangular.all('js/app/jsons/catNames.json');

		return categories.getList();
	}
}]);