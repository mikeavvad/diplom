var testApp = angular.module('testApp');

testApp.directive('myRateStarsDirective', function(){ 
	return {
		restrict: 'EA',  
		controllerUrl: 'js/app/questions/questionsController.js',
		templateUrl: 'js/components/directives/rateStars/rateStars.html',
	}
});