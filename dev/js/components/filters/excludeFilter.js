testApp.filter('excludeFilter', function ($localStorage) {
  	return function(result){
   		var filtered = _.filter(result, function(value) {
   			return +$localStorage.categoryId !== value.id;
   		});
		
		$localStorage.filtered = filtered;
        return filtered;	 
               
    }
});

