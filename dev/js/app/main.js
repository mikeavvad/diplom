
var testApp = angular.module('testApp', ['ngRoute','ui.router', "ngStorage", 'categoriesServiceModule', 'questionsServiceModule']);

var base_url = '/';
	// configure our routes
	testApp.config(function($locationProvider) {			
		$locationProvider.html5Mode(true);    
	});

	// create the controller and inject Angular's $scope