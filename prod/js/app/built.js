
var testApp = angular.module('testApp', ['ngRoute','ui.router', "ngStorage", 'categoriesServiceModule', 'questionsServiceModule']);

var base_url = '/';
	// configure our routes
	testApp.config(function($locationProvider) {			
		$locationProvider.html5Mode(true);    
	});

	// create the controller and inject Angular's $scope
var testApp = angular.module('testApp');

testApp.config(function($stateProvider) {
	$stateProvider

			// route for the home page
			.state('homeState', {
				url: '/',
				templateUrl : 'js/app/home/home.html',
				controller  : 'homeController'
			})

			// route for the about page
			.state('registrationState', {
				url: '/registration',
				templateUrl : 'js/app/registration/registration.html',
				controller  : 'registrationController'
			})

			.state('categoriesState', {
				url: '/categories',
				templateUrl : 'js/app/categories/categories.html',
				controller  : 'categoriesController'
			})

			.state('loginState', {
				url: '/login',
				templateUrl : 'js/app/login/login.html',
				controller  : 'loginController'
			})

			.state('resultsState', {
				url: '/results',
				templateUrl : 'js/app/questions/results.html',
				controller  : 'questionsController'
			})
			.state('questionState', {
				url: '/categories/:catid/question/:questionid',
				templateUrl : 'js/app/questions/question.html',
				controller: 'questionsController'
			});
			
		});
var testApp = angular.module('testApp');

testApp.controller('categoriesController', function($scope, $localStorage, categoriesService) {


	categoriesService.getCategories().then(function(categ){

		$scope.categories = categ;

		$scope.choosing = 'Choose a category:';
		$scope.result = [];

		$scope.status = 'even';

		if($localStorage.filtered.length % 2 != 0) {
			$scope.status= 'odd';
		}

	});
});

var testApp = angular.module('testApp');

testApp.controller('homeController', function($scope) {
	$scope.message = "Firstly you need to log in. Then you'll be redirected to testing page!";
});

var testApp = angular.module('testApp');

testApp.controller('loginController', function($scope, $localStorage, $state) {

	$scope.loginModel ={
		name: "Misha",
		email: "mail@mail.ru"
	}

	var users = $localStorage.users;
	$scope.message = 'To log in please enter your name and email';

	$scope.login = function(){

		angular.forEach(users, function(value, key){
			if(value.name == $scope.loginModel.name && value.email == $scope.loginModel.email ) {
				var ask = confirm('Do you wish to enter with your account or with a new one?\n tap "OK" if with this one \n tap "Cancel" if with new one');
				if(ask == true) {          			
					$state.go('categoriesState');
					$localStorage.currentUser = value.phone;
					$localStorage.categoryId = "";
				} else {
					$state.go('registrationState');
				}
			} else {
				$scope.message = 'Incorrect data!';
			}


		});

		
	}
});
var testApp = angular.module('testApp');


testApp.controller('questionsController', function($scope, $http, $stateParams, $route, $state, $localStorage, categoriesService, questionsService) {
  categoriesService.getCategories().then(function(categ){
    $scope.categories = categ;
    $scope.catid = $stateParams.catid;
    $scope.questionid = $stateParams.questionid;
    


    _.each($scope.categories, function(value){
      if(value.id == $scope.catid) {
        questionsService.getCategories('js/app/jsons/' + value.jsonName).then(function(currentJson){ 
          $scope.currentquestions = currentJson;
        });
      }    
    });


    $scope.formData = {};
    $scope.correctAnswersCount = 0;
    
    
    $scope.tempResults =  $localStorage.tempResults;
    


    _.each($localStorage.users, function(value){
     if(value.phone == $localStorage.currentUser) {
       if(value.scores == null) {
         value.scores = {};
       }  

       if(value.scores[$localStorage.categoryId] == undefined){
        $scope.previous = "You haven't passed this test yet.";
      }else {
        $scope.previous ="Last time it was: " + value.scores[$localStorage.categoryId]; 
      }
      $scope.transfer = function(){
        value.scores[$localStorage.categoryId] = $localStorage.tempResults;
        
      } 
    }   
    
  });
    

    
    $scope.nextquestion = function() {
      var mybool = true;
      
      _.each($scope.currentquestions, function(value, key){
        if(mybool) {
          if(value.id == $stateParams.questionid) {
            if($scope.formData.answer ==  $scope.currentquestions[key].correctAnswer) {
             $scope.correctAnswersCount = $scope.correctAnswersCount+1;
             $localStorage.correctAnswersCount = $scope.correctAnswersCount;
             $localStorage.categoryId = $scope.catid;
           }                   
           $localStorage.currentQuestionsNumber = $scope.currentquestions.length;
           
           if($scope.currentquestions[key+1] == null) {   

            $state.go('resultsState');
            $localStorage.tempResults = (($localStorage.correctAnswersCount / $localStorage.currentQuestionsNumber) * 100).toFixed(0);                      
            
          } else {

            $scope.questionid = $scope.currentquestions[key+1].id;
            $stateParams.questionid = $scope.questionid;        
            
          }
          
          mybool = false;
          
        }
      }
    });  

      _.each($scope.categories, function(value, key){
        if(value.id == $scope.catid) {
          $localStorage.categoryId = $scope.catid;
        }    
      });                         
    }; 

    $scope.rate = 0;
    if($scope.tempResults <= 20 && $scope.tempResults != 0){
     $scope.rate = 1;
   } else if($scope.tempResults <= 40 && $scope.tempResults != 0) {
     $scope.rate = 2;
   }else if($scope.tempResults <= 60 && $scope.tempResults != 0) {
     $scope.rate = 3;
   }else if($scope.tempResults <= 80 && $scope.tempResults != 0 ) {
     $scope.rate = 4;
   }else if($scope.tempResults <= 100 && $scope.tempResults != 0) {
     $scope.rate = 5;
   }
   

   $scope.stars = [1,2,3,4,5];
 });

});
var testApp = angular.module('testApp');

testApp.controller('registrationController', function($scope,  $localStorage, $state) {
	$scope.registrationModel = {
		phone: 0506167239,
		age: 23,
		name: 'Misha',
		email: 'mail@mail.ru'
	};
	
	 

	$scope.submit = function() {

		
			//$localStorage.users.push($scope.registrationModel);
			
			if($localStorage.users == null){
				$localStorage.users = [];
			}			
			
			if($localStorage.users.length == 0) {
				$localStorage.users.push($scope.registrationModel);
				$state.go('loginState');
			} else {
				angular.forEach($localStorage.users, function(value,key){
					if(value.email == $scope.registrationModel.email ) {
						alert('User with such email was already registered');
					}else {
						$localStorage.users.push($scope.registrationModel);
						$state.go('loginState');
					}
				});
			}
						
		}	
	});
var testApp = angular.module('testApp');

testApp.directive('myLogoDirective', function(){
	

	return {
		restrict: 'EA',
		templateUrl: 'js/components/directives/logoCanvas/logoCanvas.html',
		link: function() {
			

			var c1 = document.getElementById('c1'),
			ctx = c1.getContext("2d");

			console.log(c1);

			var img = document.createElement('img');
			img.onload = function() {
				
				ctx.fillStyle = ctx.createPattern(img, 'repeat');
				ctx.font = 'bold 350% Verdana'
				ctx.fillText("testApp", 25, 100);
			};
			img.src = "attach/img/space.jpg";



			var c2 = document.getElementById('c2'),
			ctx2 = c2.getContext("2d");

			var canvasPos = getPosition(c2);



			var mouseX = 0;
			var mouseY = 0;

			c2.addEventListener("mousemove", setMousePosition, false);
			
			document.getElementById('c2').onmouseout = function () {
				ctx2.clearRect(0, 0, c2.width, c2.height);
			}

			function setMousePosition(e) {
				mouseX = e.clientX - canvasPos.x;
				mouseY = e.clientY - canvasPos.y;
				ctx2.clearRect(0, 0, c2.width, c2.height);
				ctx2.beginPath();

				ctx2.arc(mouseX, mouseY, 5, 0, 2 * Math.PI, true);
				ctx2.fillStyle = ctx.createPattern(img, 'repeat');
				ctx2.fill();
			}	

			function update() {  			
				requestAnimationFrame(update);
			}

			function getPosition(el) {
				var xPosition = 0;
				var yPosition = 0;

				while (el) {
					xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
					yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
					el = el.offsetParent;
				}
				return {
					x: xPosition,
					y: yPosition
				};
			}
			update();

		}
	}

});
var testApp = angular.module('testApp');

testApp.directive('myRadioButtonsDirective', function(){
  return {
    restrict: 'EA',
    templateUrl: 'js/components/directives/radioButtons/radioButtons.html',
  }
});
var testApp = angular.module('testApp');

testApp.directive('myRateStarsDirective', function(){ 
	return {
		restrict: 'EA',  
		controllerUrl: 'js/app/questions/questionsController.js',
		templateUrl: 'js/components/directives/rateStars/rateStars.html',
	}
});
testApp.filter('excludeFilter', function ($localStorage) {
  	return function(result){
   		var filtered = _.filter(result, function(value) {
   			return +$localStorage.categoryId !== value.id;
   		});
		
		$localStorage.filtered = filtered;
        return filtered;	 
               
    }
});


var categ_service = angular.module('categoriesServiceModule', ['restangular']);

categ_service.service('categoriesService', ['Restangular', function(Restangular) {
	Restangular.setBaseUrl(base_url);

	this.getCategories = function() {
		var categories = Restangular.all('js/app/jsons/catNames.json');

		return categories.getList();
	}
}]);
var questions_service = angular.module('questionsServiceModule', ['restangular']);


questions_service.service('questionsService', ['Restangular', function(Restangular) {
	Restangular.setBaseUrl(base_url);

	this.getCategories = function(route) {
		var questions = Restangular.all(route);

		return questions.getList();
	}
}]);