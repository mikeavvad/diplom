var questions_service = angular.module('questionsServiceModule', ['restangular']);


questions_service.service('questionsService', ['Restangular', function(Restangular) {
	Restangular.setBaseUrl(base_url);

	this.getCategories = function(route) {
		var questions = Restangular.all(route);

		return questions.getList();
	}
}]);