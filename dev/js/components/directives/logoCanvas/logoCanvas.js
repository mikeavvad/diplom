var testApp = angular.module('testApp');

testApp.directive('myLogoDirective', function(){
	

	return {
		restrict: 'EA',
		templateUrl: 'js/components/directives/logoCanvas/logoCanvas.html',
		link: function() {
			

			var c1 = document.getElementById('c1'),
			ctx = c1.getContext("2d");

			console.log(c1);

			var img = document.createElement('img');
			img.onload = function() {
				
				ctx.fillStyle = ctx.createPattern(img, 'repeat');
				ctx.font = 'bold 350% Verdana'
				ctx.fillText("testApp", 25, 100);
			};
			img.src = "attach/img/space.jpg";



			var c2 = document.getElementById('c2'),
			ctx2 = c2.getContext("2d");

			var canvasPos = getPosition(c2);



			var mouseX = 0;
			var mouseY = 0;

			c2.addEventListener("mousemove", setMousePosition, false);
			
			document.getElementById('c2').onmouseout = function () {
				ctx2.clearRect(0, 0, c2.width, c2.height);
			}

			function setMousePosition(e) {
				mouseX = e.clientX - canvasPos.x;
				mouseY = e.clientY - canvasPos.y;
				ctx2.clearRect(0, 0, c2.width, c2.height);
				ctx2.beginPath();

				ctx2.arc(mouseX, mouseY, 5, 0, 2 * Math.PI, true);
				ctx2.fillStyle = ctx.createPattern(img, 'repeat');
				ctx2.fill();
			}	

			function update() {  			
				requestAnimationFrame(update);
			}

			function getPosition(el) {
				var xPosition = 0;
				var yPosition = 0;

				while (el) {
					xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
					yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
					el = el.offsetParent;
				}
				return {
					x: xPosition,
					y: yPosition
				};
			}
			update();

		}
	}

});