var testApp = angular.module('testApp');

testApp.controller('registrationController', function($scope,  $localStorage, $state) {
	$scope.registrationModel = {
		phone: 0506167239,
		age: 23,
		name: 'Misha',
		email: 'mail@mail.ru'
	};
	
	 

	$scope.submit = function() {

		
			//$localStorage.users.push($scope.registrationModel);
			
			if($localStorage.users == null){
				$localStorage.users = [];
			}			
			
			if($localStorage.users.length == 0) {
				$localStorage.users.push($scope.registrationModel);
				$state.go('loginState');
			} else {
				angular.forEach($localStorage.users, function(value,key){
					if(value.email == $scope.registrationModel.email ) {
						alert('User with such email was already registered');
					}else {
						$localStorage.users.push($scope.registrationModel);
						$state.go('loginState');
					}
				});
			}
						
		}	
	});