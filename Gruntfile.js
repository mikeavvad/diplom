module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
		
		
		less: {
    			files: {
      				'dev/js/app/app.css': ['dev/js/app/**/*.less','dev/js/components/**/*.less']
    			}
			
  		},
		watch: {
			options: {
			 		livereload: true,
					port: 80,
			 	},
    		 less: {
     		 	files: ['dev/js/app/**/*.less'],
      		 	tasks: ['less', 'watch'],				
    		 },
			
			 
  			},
		
		concat: {			
    		js: {
      			src: ['dev/js/app/*.js','dev/js/app/**/*.js','dev/js/components/**/*.js'],
     			dest: 'prod/js/app/built.js',
    		},
			css: {
				src: ['dev/js/app/*.css', 'dev/attach/css/*.css',],
     			dest: 'prod/js/app/built.css',
			},
			
  		},
		
		copy: {
			files: {
   				cwd: 'dev/',  // set working folder / root to copy
   				 src: ['*.html','*.json', '.htaccess'],           // copy all files and subfolders
   				 dest: 'prod/',    // destination folder
    			 expand: true
  				},
			attach: {
				 cwd: 'dev/attach/',  // set working folder / root to copy
   				 src: '**/*',           // copy all files and subfolders
   				 dest: 'prod/attach',    // destination folder
    			 expand: true           // required when using cwd				
			},
			bower: {
				 cwd: 'dev/bower_components/',  // set working folder / root to copy
   				 src: '**/*',           // copy all files and subfolders
   				 dest: 'prod/bower_components',    // destination folder
    			 expand: true  
			},
			templates: {
				cwd: 'dev/js/',  // set working folder / root to copy
   				src: '**/*.html',           // copy all files and subfolders
   				dest: 'prod/js',    // destination folder
    			expand: true 
			},
			jsons: {
				cwd: 'dev/js/app/jsons',  // set working folder / root to copy
   				src: '*',           // copy all files and subfolders
   				dest: 'prod/js/app/jsons',    // destination folder
    			expand: true 
			}
			
		},
		wiredep: {
			devDep: {
				src: [],
				options: {
					
				},
				cwd: 'dev/',
			},

		},	
		
		
		ngtemplates:    {
    		//prod: {
      		//	options:    {
        		//	module:   'template',
					// base:     'dev/js/app',
     			// },
     			// src:         ['dev/js/app/**/*.html', 'dev/js/components/**/*.html'], 
     			// dest:       'dev/js/app/templates.js'
    			//}
  			},
		
		injector: {
    				options: {
						transform: function(filePath) {
							console.log(filePath);
        				filePath = filePath.replace('/dev/', '');
						filePath = filePath.replace('/prod/', '');
							var strcss = /\.css$/;
							if (filePath.match(strcss)){
								return '<link rel="stylesheet" href="' + filePath + '">';
							}
							else {
								 return '<script src="' + filePath + '"></script>';
							}
       					
						},
      				},
				
    				devinj: {
      					files: {
       				 	'dev/index.html': ['dev/js/app/main.js','dev/js/**/*.js', 'dev/attach/**/*.css', 'dev/js/**/*.css'],
      					}
    				},
					prodinj: {
						files: {
       				 	'prod/index.html': ['prod/js/app/built.js', 'prod/js/app/built.css'],
      					}
					}
				
		  },
		
    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
	grunt.loadNpmTasks('grunt-injector');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-angular-templates');
	grunt.loadNpmTasks('grunt-wiredep');


    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['watch']);
	grunt.registerTask('build', ['less','copy', 'concat', 'injector', 'wiredep']);

};