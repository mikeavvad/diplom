var testApp = angular.module('testApp');

testApp.config(function($stateProvider) {
	$stateProvider

			// route for the home page
			.state('homeState', {
				url: '/',
				templateUrl : 'js/app/home/home.html',
				controller  : 'homeController'
			})

			// route for the about page
			.state('registrationState', {
				url: '/registration',
				templateUrl : 'js/app/registration/registration.html',
				controller  : 'registrationController'
			})

			.state('categoriesState', {
				url: '/categories',
				templateUrl : 'js/app/categories/categories.html',
				controller  : 'categoriesController'
			})

			.state('loginState', {
				url: '/login',
				templateUrl : 'js/app/login/login.html',
				controller  : 'loginController'
			})

			.state('resultsState', {
				url: '/results',
				templateUrl : 'js/app/questions/results.html',
				controller  : 'questionsController'
			})
			.state('questionState', {
				url: '/categories/:catid/question/:questionid',
				templateUrl : 'js/app/questions/question.html',
				controller: 'questionsController'
			});
			
		});