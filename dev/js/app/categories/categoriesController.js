var testApp = angular.module('testApp');

testApp.controller('categoriesController', function($scope, $localStorage, categoriesService) {


	categoriesService.getCategories().then(function(categ){

		$scope.categories = categ;

		$scope.choosing = 'Choose a category:';
		$scope.result = [];

		$scope.status = 'even';

		if($localStorage.filtered.length % 2 != 0) {
			$scope.status= 'odd';
		}

	});
});
