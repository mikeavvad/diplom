var testApp = angular.module('testApp');

testApp.directive('myRadioButtonsDirective', function(){
  return {
    restrict: 'EA',
    templateUrl: 'js/components/directives/radioButtons/radioButtons.html',
  }
});