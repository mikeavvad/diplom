var testApp = angular.module('testApp');

testApp.controller('loginController', function($scope, $localStorage, $state) {

	$scope.loginModel ={
		name: "Misha",
		email: "mail@mail.ru"
	}

	var users = $localStorage.users;
	$scope.message = 'To log in please enter your name and email';

	$scope.login = function(){

		angular.forEach(users, function(value, key){
			if(value.name == $scope.loginModel.name && value.email == $scope.loginModel.email ) {
				var ask = confirm('Do you wish to enter with your account or with a new one?\n tap "OK" if with this one \n tap "Cancel" if with new one');
				if(ask == true) {          			
					$state.go('categoriesState');
					$localStorage.currentUser = value.phone;
					$localStorage.categoryId = "";
				} else {
					$state.go('registrationState');
				}
			} else {
				$scope.message = 'Incorrect data!';
			}


		});

		
	}
});