var testApp = angular.module('testApp');


testApp.controller('questionsController', function($scope, $http, $stateParams, $route, $state, $localStorage, categoriesService, questionsService) {
  categoriesService.getCategories().then(function(categ){
    $scope.categories = categ;
    $scope.catid = $stateParams.catid;
    $scope.questionid = $stateParams.questionid;
    


    _.each($scope.categories, function(value){
      if(value.id == $scope.catid) {
        questionsService.getCategories('js/app/jsons/' + value.jsonName).then(function(currentJson){ 
          $scope.currentquestions = currentJson;
        });
      }    
    });


    $scope.formData = {};
    $scope.correctAnswersCount = 0;
    
    
    $scope.tempResults =  $localStorage.tempResults;
    


    _.each($localStorage.users, function(value){
     if(value.phone == $localStorage.currentUser) {
       if(value.scores == null) {
         value.scores = {};
       }  

       if(value.scores[$localStorage.categoryId] == undefined){
        $scope.previous = "You haven't passed this test yet.";
      }else {
        $scope.previous ="Last time it was: " + value.scores[$localStorage.categoryId]; 
      }
      $scope.transfer = function(){
        value.scores[$localStorage.categoryId] = $localStorage.tempResults;
        
      } 
    }   
    
  });
    

    
    $scope.nextquestion = function() {
      var mybool = true;
      
      _.each($scope.currentquestions, function(value, key){
        if(mybool) {
          if(value.id == $stateParams.questionid) {
            if($scope.formData.answer ==  $scope.currentquestions[key].correctAnswer) {
             $scope.correctAnswersCount = $scope.correctAnswersCount+1;
             $localStorage.correctAnswersCount = $scope.correctAnswersCount;
             $localStorage.categoryId = $scope.catid;
           }                   
           $localStorage.currentQuestionsNumber = $scope.currentquestions.length;
           
           if($scope.currentquestions[key+1] == null) {   

            $state.go('resultsState');
            $localStorage.tempResults = (($localStorage.correctAnswersCount / $localStorage.currentQuestionsNumber) * 100).toFixed(0);                      
            
          } else {

            $scope.questionid = $scope.currentquestions[key+1].id;
            $stateParams.questionid = $scope.questionid;        
            
          }
          
          mybool = false;
          
        }
      }
    });  

      _.each($scope.categories, function(value, key){
        if(value.id == $scope.catid) {
          $localStorage.categoryId = $scope.catid;
        }    
      });                         
    }; 

    $scope.rate = 0;
    if($scope.tempResults <= 20 && $scope.tempResults != 0){
     $scope.rate = 1;
   } else if($scope.tempResults <= 40 && $scope.tempResults != 0) {
     $scope.rate = 2;
   }else if($scope.tempResults <= 60 && $scope.tempResults != 0) {
     $scope.rate = 3;
   }else if($scope.tempResults <= 80 && $scope.tempResults != 0 ) {
     $scope.rate = 4;
   }else if($scope.tempResults <= 100 && $scope.tempResults != 0) {
     $scope.rate = 5;
   }
   

   $scope.stars = [1,2,3,4,5];
 });

});