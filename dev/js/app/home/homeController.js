var testApp = angular.module('testApp');

testApp.controller('homeController', function($scope) {
	$scope.message = "Firstly you need to log in. Then you'll be redirected to testing page!";
});
